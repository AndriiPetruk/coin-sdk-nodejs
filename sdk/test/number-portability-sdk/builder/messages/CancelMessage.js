export default function buildMessage(offset, builder) {
  let message;
  switch (offset) {
    case 0:
      message = builder
        .setHeader("DEMO1", "DEMO2")
        .setDossierId("DEMO1-123456")
        .setNote("Example note")
        .build();
      break;
    case 1:
      message = builder
        .setFullHeader("DEMO1", "DEMO1", "DEMO2", "DEMO2")
        .setDossierId("DEMO1-123456")
        .setNote("Example note")
        .build();
      break;
    case 2:
      message = builder
        .setFullHeader("DEMO1", "DEMO1", "DEMO2", "DEMO2")
        .setDossierId("DEMO1-123456")
        .build();
      break;
    default:
      console.error(`buildMessage: invalid offset=${offset}`);
      break;
  }
  return message;
}

