import expect from './common/expect';

import Builder from '../../../src/number-portability-sdk/messages/v3/builder/DeactivationServiceNumberBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype.js';

import results from './results/DeactivationServiceNumberMessage';
import buildMessage from './messages/DeactivationServiceNumberMessage';
import {removeUndefined} from "../../common-sdk/utils";

function fixResult(result, actual) {
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('DeactivationServiceNumberBuilder', () => {
  it('message should be of type deactivationsn', () => {
    const builder = new Builder();
    const message = buildMessage(0, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.deactivationsn);
  });

  it('message should have the correct structure', () => {
    const offset = 0;
    const message = buildMessage(offset, new Builder());
    const expected = results[offset];
    const actual = removeUndefined(message.getMessage());

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
