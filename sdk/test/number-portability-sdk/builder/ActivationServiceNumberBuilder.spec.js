import expect from './common/expect';

import ActivationServiceNumberBuilder from '../../../src/number-portability-sdk/messages/v3/builder/ActivationServiceNumberBuilder';
import MessageTypeEnum from '../../../src/number-portability-sdk/messages/v3/messagetype';

import results from './results/ActivationServiceNumberMessage';
import buildMessage from './messages/ActivationServiceNumberMessage';
import {removeUndefined} from "../../common-sdk/utils";

function fixResult(result, actual) {
  result.message.body.activationsn.planneddatetime = actual.message.body.activationsn.planneddatetime;
  result.message.header.timestamp = actual.message.header.timestamp;
  return result;
}

describe('ActiviationServiceNumberBuilder', () => {
  it('message should be of type activationsn', () => {
    const offset = 0;
    const builder = new ActivationServiceNumberBuilder();
    const message = buildMessage(offset, builder);

    expect(message.getMessageType()).to.equal(MessageTypeEnum.activationsn);
  });

  it('message should have correct structure', () => {
    const offset = 0;
    const builder = new ActivationServiceNumberBuilder();
    const message = buildMessage(offset, builder);
    const expected = results[offset];
    const actual = removeUndefined(message.getMessage());

    expect(actual).to.deep.equalInAnyOrder(fixResult(expected, actual));
  });
});
