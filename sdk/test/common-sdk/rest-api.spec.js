import RestApiClient from '../../src/common-sdk/rest-api';
import { baseUrl, privateKeyFile, encryptedHmacSecretFile } from "../index";
import got from "got";

const consumerName = "loadtest-loada";
const validPeriodInSeconds = 30;

describe('rest-api', () => {
  it('should connect with backend',   (done) => {
    const api = new RestApiClient(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds);
    const url = baseUrl+"/number-portability/v3/dossiers/events";
    const req = got.get(
      url,
      {
        headers: {
          ...api.generateHeaders('GET', url),
          Accept: 'text/event-stream'
        }
      }
    );
    req.on('response', () => {
      req.cancel();
      done();
    });
  });
});
