export function removeUndefined(obj) {
  Object.keys(obj).forEach(key => {
    if(typeof obj[key] === 'object') {
      removeUndefined(obj[key]);
    } else if(obj[key] === undefined) {
      delete obj[key];
    }
  });
  return obj;
}
