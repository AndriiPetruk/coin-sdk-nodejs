import utils from '../../src/common-sdk/utils';
import chai from 'chai';
const { expect } = chai;

function resolveAfterNCalls(n) {
  let cnt = n;
  const deferred = new utils.Deferred();
  return {
    promise: deferred.promise,
    callback: () => {
      cnt--;
      if (cnt === 0)
        deferred.resolve();
    }
  };
}

describe('utils', () => {
  it('createUrl should create proper urls', () => {
    expect(utils.createUrl("https://base.com:4531")).to.equal("https://base.com:4531");
    expect(utils.createUrl("https://base.com:4531",{key:'value'})).to.equal("https://base.com:4531?key=value");
    expect(utils.createUrl("https://base.com:4531",{key:null})).to.equal("https://base.com:4531");
    expect(utils.createUrl("https://base.com:4531",{key:undefined})).to.equal("https://base.com:4531");
    expect(utils.createUrl("https://base.com:4531",{key:'encoded=bla'})).to.equal("https://base.com:4531?key=encoded%3Dbla");
    expect(utils.createUrl("https://base.com:4531",{key:'encoded=bla',another:'{"json":1234}'})).to.equal("https://base.com:4531?key=encoded%3Dbla&another=%7B%22json%22%3A1234%7D");
  });
  it('formatDate should create proper date strings', () => {
    expect(utils.formatDate(new Date(2021,0,1,0,0,0))).to.equal("20210101000000");
    expect(utils.formatDate(new Date(2021,11,31,23,59,59))).to.equal("20211231235959");
    expect(utils.formatDate(new Date(2021,1,9,7,6,3))).to.equal("20210209070603");
  });
  describe('createRetryer', () => {
    it('should return object with retry and reset', () => {
      const retryer = utils.createRetryer();
      expect(retryer.retry).to.exist;
      expect(retryer.reset).to.exist;
    });
    describe('retry', () => {
      it('should return true when still retrying', () => {
        const retryer = utils.createRetryer(3,1);
        const callback = () => {callback.called = (callback.called || 0) + 1;};
        expect(retryer.retry(callback)).be.true;
      });
      it('should call the callback', (done) => {
        const retryer = utils.createRetryer(3,1);
        retryer.retry(done);
      });
      it('should return false if amount of retries is exceeeded', (done) => {
        const retryer = utils.createRetryer(3,1);
        const resolver = resolveAfterNCalls(3);
        retryer.retry(resolver.callback);
        retryer.retry(resolver.callback);
        retryer.retry(resolver.callback);
        expect(retryer.retry(resolver.callback)).be.false;
        resolver.promise.then(done);
      });
    });
    describe('reset', () => {
      it('should reset the amount of retries', (done) => {
        const retryer = utils.createRetryer(3,1);
        const resolver = resolveAfterNCalls(3);
        retryer.retry(resolver.callback);
        retryer.retry(resolver.callback);
        retryer.retry(resolver.callback);
        resolver.promise.then(()=>{
          retryer.reset();
          expect(retryer.retry(done)).be.true;
        });
      });
    });
  });
  describe('createReadTimeoutCheck', () => {
    it('should return object with start, stop and reset', () => {
      const check = utils.createReadTimeoutCheck(10);
      expect(check.start).to.exist;
      expect(check.stop).to.exist;
      expect(check.reset).to.exist;
    });
    describe('start', () => {
      it('should call the reconnectFn eventually', (done) => {
        const check = utils.createReadTimeoutCheck(10);
        check.start(()=>{
          done();
        });
      });
    });
    describe('stop', () => {
      it('should not call the reconnectFn', (done) => {
        const check = utils.createReadTimeoutCheck(10);
        check.start(()=>{expect(true).be.false;});
        check.stop();
        setTimeout(done,100);
      });
    });
    describe('reset', () => {
      it('should hold off calling the reconnectFn', (done) => {
        const check = utils.createReadTimeoutCheck(10);
        let passed = false;
        check.start(()=>{
          expect(passed).be.true;
          done();
        });
        setTimeout(()=>{
          check.reset();
          setTimeout(()=>{
            check.reset();
            setTimeout(()=>{
              check.reset();
              setTimeout(()=>{
                passed = true;
              },5);
            },5);
          },5);
        },5);
      });
    });
  });
});
