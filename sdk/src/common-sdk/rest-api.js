import Security from './security';

export default class RestApiClient {
  constructor(consumerName, privateKeyFile, encryptHmacSecretFile, validPeriodInSeconds = 30) {
    this.security = new Security(consumerName, privateKeyFile, encryptHmacSecretFile, validPeriodInSeconds);
  }

  //Release script will update the version number, changing it in the code will break this.
  static getFullVersion() {
    return 'coin-sdk-nodejs-1.1.2';
  }

  generateHeaders(method, url, content = '') {
    return {
      ...this.security.generateHeaders(
        method,
        url,
        content,
      ),
      "User-Agent": RestApiClient.getFullVersion(),
    };
  }
}
