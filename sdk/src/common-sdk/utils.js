import fs from 'fs';

class Deferred {
  constructor() {
    this.resolve = null;
    this.reject = null;
    this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
    Object.freeze(this);
  }
}

function getQueryParams(params) {
  return Object
    .keys(params)
    .filter(key => params[key] !== undefined && params[key] !== null)
    .map(key => encodeURIComponent(key)+"="+encodeURIComponent(params[key]))
    .join("&");
}

export default {
  formatDate(date) {
    return date.getFullYear() +
    ("0" + (date.getMonth() + 1)).slice(-2) +
    ("0" + date.getDate()).slice(-2) +
    ("0" + date.getHours()).slice(-2) +
    ("0" + date.getMinutes()).slice(-2) +
    ("0" + date.getSeconds()).slice(-2);
  },

  getFileContent(path) {
    return fs.readFileSync(path, 'utf-8');
  },

  timestamp() {
    return (new Date()).toJSON().replace(/[T:-]/g, '').split('.')[0];
  },

  createReadTimeoutCheck(maxTimeBetweenData = 40000) {
    let timer;
    let lastMessage = Date.now();
    const scheduleCheck = (reconnectFn) => {
      const delay = Math.max(1,(lastMessage + maxTimeBetweenData) - Date.now());
      timer = setTimeout(()=>{
        if (lastMessage + maxTimeBetweenData < Date.now())
          reconnectFn();
        else
          scheduleCheck(reconnectFn);
      }, delay);
    };
    return {
      start: scheduleCheck,
      stop: () => clearTimeout(timer),
      reset: () => { lastMessage = Date.now(); }
    };
  },

  createRetryer(initialRetries = 3, initialBackoff = 1000, maxBackoff = 60000) {
    let retriesLeft = initialRetries;
    let backoff = initialBackoff;
    let timer;
    return {
      retry: (fn) => {
        if (retriesLeft < 1)
          return false;
        retriesLeft -= 1;
        timer = setTimeout(fn, backoff);
        backoff = Math.min(maxBackoff, backoff * 2);
        return true;
      },
      reset: () => {
        retriesLeft = initialRetries;
        backoff = initialBackoff;
        clearTimeout(timer);
      }
    };
  },

  createUrl(uri, params = {}) {
    const query = getQueryParams(params);
    if (query)
      return uri + "?" + query;
    return uri;
  },

  Deferred
};
