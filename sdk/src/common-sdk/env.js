import fs from 'fs';
import dotenv from 'dotenv';

dotenv.config();

const ENVFILE = '.env';

const keys = [
  { name: 'BASE_URL', type: 'string', value: null, default: null },
  { name: 'CONSUMER_NAME', type: 'string', value: null, default: null },
  { name: 'KEEP_ALIVE', type: 'boolean', value: null, default: true },
  { name: 'KEEP_ALIVE_MSECS', type: 'number', value: null, default: 2000 },
  { name: 'MAX_SOCKETS', type: 'number', value: null, default: 50 },
  { name: 'REJECT_UNAUTHORIZED', type: 'boolean', value: null, default: false },
  { name: 'REQUEST_CERT', type: 'boolean', value: null, default: true },
  { name: 'LOG_LEVEL', type: 'number', value: null, default: 1 },
  { name: 'PRIVATE_KEY_FILE', type: 'string', value: null, default: null },
  { name: 'SHARED_SECRET_FILE', type: 'string', value: null, default: null }
];

let uninitialized = true;
let found = true;

function convertString(value) {
  return value.toString();
}

function convertNumber(value) {
  if (!isNaN(value)) {
    return +value;
  } else {
    console.error(`env.convertNumber(value='${value}') value is not a number`);
    return 0;
  }
}

function convertBoolean(value) {
  if (value.match(/^(true|on|1|yes|enabled)$/i)) {
    return true;
  } else if (value.match(/^(false|off|0|no|disabled)$/i)) {
    return false;
  } else {
    console.error(`env.convertBoolean(value='${value}') value is not a boolean`);
    return false;
  }
}

function convert(value, type) {
  if (type === 'string') {
    return convertString(value);
  } else if (type === 'number') {
    return convertNumber(value);
  } else if (type === 'boolean') {
    return convertBoolean(value);
  } else {
    console.error(`env.convert(value='${value}',type='${type}') unknown type`);
    return null;
  }
}

export default {
  init() {
    if (!fs.existsSync(ENVFILE)) {
      console.warn(`Cannot find '${ENVFILE}' file, using defaults`);
      found = false;
    }
    keys.forEach(key => {
      if (process.env[key.name]) {
        key.value = convert(process.env[key.name], key.type);
      } else if (found) {
        console.warn(`env.init() key.name='${key.name}' not found, using default`);
      }
    });
    uninitialized = false;
  },

  get(name) {
    if (uninitialized) {
      this.init();
    }
    const key = keys.find(k => k.name === name);
    if (key) {
      return key.value || key.default;
    } else {
      console.warn(`env.get(name='${name}') returns null`);
      return null;
    }
  }
};
