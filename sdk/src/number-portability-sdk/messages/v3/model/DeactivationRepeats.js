/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import DeactivationSeq from './DeactivationSeq';

/**
* The DeactivationRepeats model module.
* @module model/DeactivationRepeats
* @version 3.0.0
*/
export default class DeactivationRepeats {
    /**
    * Constructs a new <code>DeactivationRepeats</code>.
    * @alias module:model/DeactivationRepeats
    * @class
    * @param seq {module:model/DeactivationSeq}
    */

    constructor(seq) {


        this['seq'] = seq;

    }

    /**
    * Constructs a <code>DeactivationRepeats</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/DeactivationRepeats} obj Optional instance to populate.
    * @return {module:model/DeactivationRepeats} The populated <code>DeactivationRepeats</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new DeactivationRepeats();


            if (data.hasOwnProperty('seq')) {
                obj['seq'] = DeactivationSeq.constructFromObject(data['seq']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/DeactivationSeq} seq
    */
    'seq' = undefined;




}
