/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import EnumProfileDeactivation from './EnumProfileDeactivation';

/**
* The EnumProfileDeactivationBody model module.
* @module model/EnumProfileDeactivationBody
* @version 3.0.0
*/
export default class EnumProfileDeactivationBody {
    /**
    * Constructs a new <code>EnumProfileDeactivationBody</code>.
    * @alias module:model/EnumProfileDeactivationBody
    * @class
    * @param enumprofiledeactivation {module:model/EnumProfileDeactivation}
    */

    constructor(enumprofiledeactivation) {


        this['enumprofiledeactivation'] = enumprofiledeactivation;

    }

    /**
    * Constructs a <code>EnumProfileDeactivationBody</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/EnumProfileDeactivationBody} obj Optional instance to populate.
    * @return {module:model/EnumProfileDeactivationBody} The populated <code>EnumProfileDeactivationBody</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new EnumProfileDeactivationBody();


            if (data.hasOwnProperty('enumprofiledeactivation')) {
                obj['enumprofiledeactivation'] = EnumProfileDeactivation.constructFromObject(data['enumprofiledeactivation']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/EnumProfileDeactivation} enumprofiledeactivation
    */
    'enumprofiledeactivation' = undefined;




}
