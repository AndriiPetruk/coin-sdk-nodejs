/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
* The TariffInfo model module.
* @module model/TariffInfo
* @version 3.0.0
*/
export default class TariffInfo {
    /**
    * Constructs a new <code>TariffInfo</code>.
    * @alias module:model/TariffInfo
    * @class
    * @param peak {String}
    * @param offpeak {String}
    * @param currency {module:model/TariffInfo.CurrencyEnum}
    * @param type {String}
    * @param vat {String}
    */

    constructor(peak, offpeak, currency, type, vat) {


        this['peak'] = peak;
        this['offpeak'] = offpeak;
        this['currency'] = currency;
        this['type'] = type;
        this['vat'] = vat;

    }

    /**
    * Constructs a <code>TariffInfo</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/TariffInfo} obj Optional instance to populate.
    * @return {module:model/TariffInfo} The populated <code>TariffInfo</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new TariffInfo();


            if (data.hasOwnProperty('peak')) {
                obj['peak'] = ApiClient.convertToType(data['peak'], 'String');
            }
            if (data.hasOwnProperty('offpeak')) {
                obj['offpeak'] = ApiClient.convertToType(data['offpeak'], 'String');
            }
            if (data.hasOwnProperty('currency')) {
                obj['currency'] = ApiClient.convertToType(data['currency'], 'String');
            }
            if (data.hasOwnProperty('type')) {
                obj['type'] = ApiClient.convertToType(data['type'], 'String');
            }
            if (data.hasOwnProperty('vat')) {
                obj['vat'] = ApiClient.convertToType(data['vat'], 'String');
            }
        }
        return obj;
    }

    /**
    * @member {String} peak
    */
    'peak' = undefined;
    /**
    * @member {String} offpeak
    */
    'offpeak' = undefined;
    /**
    * @member {module:model/TariffInfo.CurrencyEnum} currency
    */
    'currency' = undefined;
    /**
    * @member {String} type
    */
    'type' = undefined;
    /**
    * @member {String} vat
    */
    'vat' = undefined;



    /**
    * Allowed values for the <code>currency</code> property.
    * @enum {String}
    * @readonly
    */
    static CurrencyEnum = {
        /**
         * value: "0"
         * @const
         */
        "0": "0",
        /**
         * value: "1"
         * @const
         */
        "1": "1"    };

}
