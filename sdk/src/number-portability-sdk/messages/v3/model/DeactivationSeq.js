/**
 * COIN Number Portability API V3
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 3.0.0
 * Contact: servicedesk@coin.nl
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import NumberSeries from './NumberSeries';

/**
* The DeactivationSeq model module.
* @module model/DeactivationSeq
* @version 3.0.0
*/
export default class DeactivationSeq {
    /**
    * Constructs a new <code>DeactivationSeq</code>.
    * @alias module:model/DeactivationSeq
    * @class
    * @param numberseries {module:model/NumberSeries}
    */

    constructor(numberseries) {


        this['numberseries'] = numberseries;

    }

    /**
    * Constructs a <code>DeactivationSeq</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/DeactivationSeq} obj Optional instance to populate.
    * @return {module:model/DeactivationSeq} The populated <code>DeactivationSeq</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new DeactivationSeq();


            if (data.hasOwnProperty('numberseries')) {
                obj['numberseries'] = NumberSeries.constructFromObject(data['numberseries']);
            }
        }
        return obj;
    }

    /**
    * @member {module:model/NumberSeries} numberseries
    */
    'numberseries' = undefined;




}
