import utils from '../../../../common-sdk/utils';
import EventSource from 'eventsource';
import RestApiClient from '../../../../common-sdk/rest-api';
import MessageTypesEnum from '../messagetype';

class NumberPortabilityMessageConsumer extends RestApiClient {

  constructor(consumerName = null, privateKeyFile = null, encryptedHmacSecretFile = null, baseUrl = null, backOffPeriod = 30, numberOfRetries = 10, validPeriodInSeconds = 30) {
    super(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds);
    this.sseUri = baseUrl + '/number-portability/v3/dossiers/events';
    this.retryer = utils.createRetryer(numberOfRetries, backOffPeriod, 60000);
  }

  startConsuming(listener, options = {}) {
    const state = {
      deferred: new utils.Deferred(),
      timeoutCheck: utils.createReadTimeoutCheck(),
      sseSource: undefined
    };

    const connectSse = () => {
      const confirmationStatus = options.confirmationStatus || "Unconfirmed";
      const offset = options.offsetPersister && options.offsetPersister.persistedOffset;
      const url = utils.createUrl(this.sseUri, {offset, confirmationStatus, messageTypes: options.messageTypes});
      const headers = this.generateHeaders("GET", url);
      return new EventSource(url, { headers });
    };

    const setupHandlers = (sseSource, reconnectFn) => {
      sseSource.onerror = () => {
        reconnectFn();
      };
      sseSource.onopen = () => {
        this.retryer.reset();
      };
      sseSource.onmessage = () => { // onmessage is called when an untyped message arrives (currently only heartbeats)
        state.timeoutCheck.reset();
        if (listener.onKeepAlive)
          listener.onKeepAlive();
      };
      function handler(event) { // handler is called when a typed message arrives
        state.timeoutCheck.reset();
        const handlerName = listenerEventHandleMap[event.type];
        if (handlerName)
          listener[handlerName](event);
        else if (listener.onUnknownMessage)
          listener.onUnknownMessage(event);
        if (options.offsetPersister)
          options.offsetPersister.persistOffset(event.lastEventId);
      }
      Object.keys(MessageTypesEnum).map(key => sseSource.addEventListener(key+'-v3', handler));
    };

    const reconnect = (error) => {
      state.timeoutCheck.stop();
      state.sseSource.close();
      console.error(`Consumer.getMessages() An error occurred in the server-sent event stream: ${JSON.stringify(error)}`);
      if (!this.retryer.retry(() => connect()))
        state.deferred.reject(error);
    };

    const connect = () => {
      state.sseSource = connectSse();
      setupHandlers(state.sseSource, reconnect);
      state.timeoutCheck.start(reconnect);
    };

    if (options.confirmationStatus && options.confirmationStatus !== "Unconfirmed" && !options.offsetPersister)
      throw new Error("offsetPersister is required in options when confirmationStatus isn't equal to Unconfirmed");
    else
      connect();

    return {
      close: () => {
        if (state.sseSource.readyState === 2) // already closed
          return;
        this.retryer.reset();
        state.timeoutCheck.stop();
        state.sseSource.close();
        state.deferred.resolve();
      },
      promise: state.deferred.promise
    };
  }
}

const listenerEventHandleMap = {
  [MessageTypesEnum.activationsn + "-v3"]: "onActivationsn",
  [MessageTypesEnum.cancel + "-v3"]: "onCancel",
  [MessageTypesEnum.deactivation + "-v3"]: "onDeactivation",
  [MessageTypesEnum.deactivationsn + "-v3"]: "onDeactivationsn",
  [MessageTypesEnum.enumactivationnumber + "-v3"]: "onEnumActivationNumber",
  [MessageTypesEnum.enumactivationoperator + "-v3"]: "onEnumActivationOperator",
  [MessageTypesEnum.enumactivationrange + "-v3"]: "onEnumActivationRange",
  [MessageTypesEnum.enumdeactivationnumber + "-v3"]: "onEnumDeactivationNumber",
  [MessageTypesEnum.enumdeactivationoperator + "-v3"]: "onEnumDeactivationOperator",
  [MessageTypesEnum.enumdeactivationrange + "-v3"]: "onEnumDeactivationRange",
  [MessageTypesEnum.enumprofileactivation + "-v3"]: "onEnumProfileActivation",
  [MessageTypesEnum.enumprofiledeactivation + "-v3"]: "onEnumProfileDeactivation",
  [MessageTypesEnum.errorfound + "-v3"]: "onErrorFound",
  [MessageTypesEnum.portingperformed + "-v3"]: "onPortingPerformed",
  [MessageTypesEnum.portingrequest + "-v3"]: "onPortingRequest",
  [MessageTypesEnum.portingrequestanswer + "-v3"]: "onPortingRequestAnswer",
  [MessageTypesEnum.pradelayed + "-v3"]: "onPortingRequestAnswerDelayed",
  [MessageTypesEnum.rangeactivation + "-v3"]: "onRangeActivation",
  [MessageTypesEnum.rangedeactivation + "-v3"]: "onRangeDeactivation",
  [MessageTypesEnum.tariffchangesn + "-v3"]: "onTariffChangeSn"
};

export default NumberPortabilityMessageConsumer;
