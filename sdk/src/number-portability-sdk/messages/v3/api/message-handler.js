import MessageTypesEnum from '../messagetype';

class MessageHandler {

  constructor() {
    this.handlers = [];
    for (const messageTypesKey in MessageTypesEnum) {
      const key = messageTypesKey + "-v3";
      this.handlers[key] = this.onMessage;
    }
  }

  getHandlers() {
    return this.handlers;
  }

  onMessage(event, offsetPersister, listener) {
    switch (event.type) {
      case MessageTypesEnum.activationsn + "-v3": listener.onActivationsn(event); break;
      case MessageTypesEnum.cancel + "-v3": listener.onCancel(event); break;
      case MessageTypesEnum.deactivation + "-v3": listener.onDeactivation(event); break;
      case MessageTypesEnum.deactivationsn + "-v3": listener.onDeactivationsn(event); break;
      case MessageTypesEnum.enumactivationnumber + "-v3": listener.onEnumActivationNumber(event); break;
      case MessageTypesEnum.enumactivationoperator + "-v3": listener.onEnumActivationOperator(event); break;
      case MessageTypesEnum.enumactivationrange + "-v3": listener.onEnumActivationRange(event); break;
      case MessageTypesEnum.enumdeactivationnumber + "-v3": listener.onEnumDeactivationNumber(event); break;
      case MessageTypesEnum.enumdeactivationoperator + "-v3": listener.onEnumDeactivationOperator(event); break;
      case MessageTypesEnum.enumdeactivationrange + "-v3": listener.onEnumDeactivationRange(event); break;
      case MessageTypesEnum.enumprofileactivation + "-v3": listener.onEnumProfileActivation(event); break;
      case MessageTypesEnum.enumprofiledeactivation + "-v3": listener.onEnumProfileDeactivation(event); break;
      case MessageTypesEnum.portingperformed + "-v3": listener.onPortingPerformed(event); break;
      case MessageTypesEnum.portingrequest + "-v3": listener.onPortingRequest(event); break;
      case MessageTypesEnum.portingrequestanswer + "-v3": listener.onPortingRequestAnswer(event); break;
      case MessageTypesEnum.pradelayed + "-v3": listener.onPortingRequestAnswerDelayed(event); break;
      case MessageTypesEnum.rangeactivation + "-v3": listener.onRangeActivation(event); break;
      case MessageTypesEnum.rangedeactivation + "-v3": listener.onRangeDeactivation(event); break;
      case MessageTypesEnum.tariffchangesn + "-v3": listener.onTariffChangeSn(event); break;

    }
    offsetPersister.persistOffset(event.lastEventId);
  }
}

export default MessageHandler;
