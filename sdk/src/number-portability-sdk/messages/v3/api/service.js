import RestApiClient from '../../../../common-sdk/rest-api';
import ConfirmationMessage from '../model/ConfirmationMessage';
import got from "got";

const NUMBER_PORTABILITY_URL = '/number-portability/v3';

class NumberPortabilityService extends RestApiClient {

  constructor(consumerName, privateKeyFile = null, encryptedHmacSecretFile = null, validPeriodInSeconds = 30, baseUrl) {
    super(consumerName, privateKeyFile, encryptedHmacSecretFile, validPeriodInSeconds);
    this.apiUrl = baseUrl + NUMBER_PORTABILITY_URL;
  }

  sendConfirmation(id) {
    const confirmationMessage = new ConfirmationMessage(id);
    const url = `${this.apiUrl}/dossiers/confirmations/${id}`;
    return got.put(url, {
      json: confirmationMessage,
      headers: this.generateHeaders('PUT', url, confirmationMessage)
    });
  }

  sendMessage(message) {
    return this.postMessage(message.getMessage(), message.getMessageType());
  }

  postMessage(message, messageType) {
    const url = `${this.apiUrl}/dossiers/${messageType}`;
    return got.post(url, {
      json: message,
      headers: this.generateHeaders('POST', url, message),
      responseType: 'json'
    }).then(obj => obj.body);
  }
}

export default NumberPortabilityService;
