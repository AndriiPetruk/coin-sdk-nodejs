import TariffChangeServiceNumberSeq from '../model/TariffChangeServiceNumberSeq';
import TariffChangeServiceNumberRepeats from '../model/TariffChangeServiceNumberRepeats';
import TariffInfo from '../model/TariffInfo';
import NumberSeries from '../model/NumberSeries';

class PortingRequestSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.tariffchangesnseq = new TariffChangeServiceNumberSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.tariffchangesnseq.numberseries = numberSeries;

    return this;
  }

  setTariffInfo(peak, offpeak, currency, type, vat) {
    this.tariffchangesnseq.tariffinfonew = new TariffInfo();
    this.tariffchangesnseq.tariffinfonew.peak = peak;
    this.tariffchangesnseq.tariffinfonew.offpeak = offpeak;
    this.tariffchangesnseq.tariffinfonew.currency = currency;
    this.tariffchangesnseq.tariffinfonew.type = type;
    this.tariffchangesnseq.tariffinfonew.vat = vat;

    return this;
  }


  finish() {
    this.parent.tariffchangesn.repeats.push(new TariffChangeServiceNumberRepeats(this.tariffchangesnseq));
    return this.parent;
  }
}

export default PortingRequestSequenceBuilder;


