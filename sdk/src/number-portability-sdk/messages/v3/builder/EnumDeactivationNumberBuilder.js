import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import EnumNumberSequenceBuilder from './EnumNumberSequenceBuilder';
import EnumDeactivationNumberMessage from '../model/EnumDeactivationNumberMessage';
import EnumDeactivationNumberBody from '../model/EnumDeactivationNumberBody';
import EnumContent from '../model/EnumContent';

class EnumDeactivationNumberBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumcontent = new EnumContent();
    this.enumcontent.repeats = [];
  }

  setDossierId(dossierId) {
    this.enumcontent.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumcontent.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumcontent.typeofnumber = typeOfNumber;
    return this;
  }

  addEnumNumberSequence() {
    return new EnumNumberSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new EnumDeactivationNumberBody(this.enumcontent);
    this.message = new EnumDeactivationNumberMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumdeactivationnumber);
  }
}

export default EnumDeactivationNumberBuilder;


