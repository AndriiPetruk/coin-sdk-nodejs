import PortingRequestAnswerRepeats from '../model/PortingRequestAnswerRepeats';
import PortingRequestAnswerSeq from '../model/PortingRequestAnswerSeq';
import NumberSeries from '../model/NumberSeries';

class PortingRequestAnswerSequenceBuilder {

  constructor(parent) {
    this.parent = parent;
    this.portingrequestanswerseq = new PortingRequestAnswerSeq();
  }

  setNumberSeries(start, end) {
    const numberSeries = new NumberSeries();
    numberSeries.start = start;
    numberSeries.end = end;
    this.portingrequestanswerseq.numberseries = numberSeries;

    return this;
  }

  setBlockingCode(blockingCode) {
    this.portingrequestanswerseq.blockingcode = blockingCode;
    return this;
  }

  setFirstPossibleDate(firstPossibleDate) {
    this.portingrequestanswerseq.firstpossibledate = firstPossibleDate;
    return this;
  }

  setDonorNetworkOperator(donorNetworkOperator) {
    this.portingrequestanswerseq.donornetworkoperator = donorNetworkOperator;
    return this;
  }

  setDonorServiceProvider(donorServiceProvider) {
    this.portingrequestanswerseq.donorserviceprovider = donorServiceProvider;
    return this;
  }

  setNote(note) {
    this.portingrequestanswerseq.note = note;
    return this;
  }

  finish() {
    this.parent.portingrequestanswer.repeats.push(new PortingRequestAnswerRepeats(this.portingrequestanswerseq));
    return this.parent;
  }
}

export default PortingRequestAnswerSequenceBuilder;


