import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import EnumNumberSequenceBuilder from './EnumNumberSequenceBuilder';
import EnumActivationRangeMessage from '../model/EnumActivationRangeMessage';
import EnumActivationRangeBody from '../model/EnumActivationRangeBody';
import EnumContent from '../model/EnumContent';

class EnumActivationRangeBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumcontent = new EnumContent();
    this.enumcontent.repeats = [];
  }

  setDossierId(dossierId) {
    this.enumcontent.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumcontent.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumcontent.typeofnumber = typeOfNumber;
    return this;
  }

  addEnumNumberSequence() {
    return new EnumNumberSequenceBuilder(this);
  }

  build() {
    super.build();
    this.body = new EnumActivationRangeBody(this.enumcontent);
    this.message = new EnumActivationRangeMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumactivationrange);
  }
}

export default EnumActivationRangeBuilder;


