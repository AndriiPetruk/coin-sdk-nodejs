import Message from '../message';
import MessageTypeEnum from '../messagetype';
import MessageBuilder from './MessageBuilder';
import EnumProfileActivation from '../model/EnumProfileActivation';
import EnumProfileActivationBody from '../model/EnumProfileActivationBody';
import EnumProfileActivationMessage from '../model/EnumProfileActivationMessage';

class EnumProfileActivationBuilder extends MessageBuilder {

  constructor() {
    super();
    this.enumprofileactivation = new EnumProfileActivation();
  }

  setDossierId(dossierId) {
    this.enumprofileactivation.dossierid = dossierId;
    return this;
  }

  setCurrentNetworkOperator(currentNetworkOperator) {
    this.enumprofileactivation.currentnetworkoperator = currentNetworkOperator;
    return this;
  }

  setTypeOfNumber(typeOfNumber) {
    this.enumprofileactivation.typeofnumber = typeOfNumber;
    return this;
  }

  setScope(scope) {
    this.enumprofileactivation.scope = scope;
    return this;
  }

  setProfileId(profileId) {
    this.enumprofileactivation.profileid = profileId;
    return this;
  }

  setTtl(ttl) {
    this.enumprofileactivation.ttl = ttl;
    return this;
  }

  setDnsClass(dnsClass) {
    this.enumprofileactivation.dnsclass = dnsClass;
    return this;
  }

  setRecType(recType) {
    this.enumprofileactivation.rectype = recType;
    return this;
  }

  setOrder(order) {
    this.enumprofileactivation.order = order;
    return this;
  }

  setPreference(preference) {
    this.enumprofileactivation.preference = preference;
    return this;
  }

  setFlags(flags) {
    this.enumprofileactivation.flags = flags;
    return this;
  }

  setEnumService(enumService) {
    this.enumprofileactivation.enumservice = enumService;
    return this;
  }

  setRegExp(regExp) {
    this.enumprofileactivation.regexp = regExp;
    return this;
  }

  setUserTag(userTag) {
    this.enumprofileactivation.usertag = userTag;
    return this;
  }

  setDomain(domain) {
    this.enumprofileactivation.domain = domain;
    return this;
  }

  setSpCode(spCode) {
    this.enumprofileactivation.spcode = spCode;
    return this;
  }

  setProcessType(processType) {
    this.enumprofileactivation.processType = processType;
    return this;
  }

  setGateway(gateway) {
    this.enumprofileactivation.gateway = gateway;
    return this;
  }

  setService(service) {
    this.enumprofileactivation.service = service;
    return this;
  }

  setDomainTag(domainTag) {
    this.enumprofileactivation.domaintag = domainTag;
    return this;
  }

  setReplacement(replacement) {
    this.enumprofileactivation.replacement = replacement;
    return this;
  }

  build() {
    super.build();

    this.body = new EnumProfileActivationBody(this.enumprofileactivation);
    this.message = new EnumProfileActivationMessage(this.header, this.body);

    return new Message({"message":this.message}, MessageTypeEnum.enumprofileactivation);
  }
}

export default EnumProfileActivationBuilder;
