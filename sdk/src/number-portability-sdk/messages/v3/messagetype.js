const MessageTypeEnum = {
  "activationsn": "activationsn",
  "cancel": "cancel",
  "deactivation": "deactivation",
  "deactivationsn": "deactivationsn",
  "enumactivationnumber": "enumactivationnumber",
  "enumactivationoperator": "enumactivationoperator",
  "enumactivationrange": "enumactivationrange",
  "enumdeactivationnumber": "enumdeactivationnumber",
  "enumdeactivationoperator": "enumdeactivationoperator",
  "enumdeactivationrange": "enumdeactivationrange",
  "enumprofileactivation": "enumprofileactivation",
  "enumprofiledeactivation": "enumprofiledeactivation",
  "errorfound": "errorfound",
  "portingperformed": "portingperformed",
  "portingrequest": "portingrequest",
  "portingrequestanswer": "portingrequestanswer",
  "pradelayed": "pradelayed",
  "rangeactivation": "rangeactivation",
  "rangedeactivation": "rangedeactivation",
  "tariffchangesn": "tariffchangesn"
};

export default MessageTypeEnum;
