const today = new Date();
const timestamp = today.getFullYear() +
  ("0" + (today.getMonth() + 1)).slice(-2) +
  ("0" + today.getDate()).slice(-2) +
  ("0" + today.getHours() + 1 ).slice(-2) +
  ("0" + today.getMinutes()).slice(-2) +
  ("0" + today.getSeconds()).slice(-2);

module.exports = Object.freeze({
  // Timestamp of the date/time of the moment the example is run
  TODAY: timestamp,

  // The transaction-id returned by the API
  REGEX_TRANSACTION_ID: /^Root=1-[0-9a-z]{8}-[0-9a-z]{24}$/,

  // Demo Sender/Receiver networkoperator and serviceproviders codes.
  SENDER_NETWORK_OPERATOR: "TST01",
  SENDER_SERVICE_PROVIDER: "TST01",
  RECEIVER_NETWORK_OPERATOR: "TST02",
  RECEIVER_SERVICE_PROVIDER: "TST02",

  // Demo dossier-id as an example, in real world scenario's. The id should be generated an unique (in case of a PortingRequest)
  // or the dossier-id should match the received dossier-id (in case of a PortingRequestAnswer)
  DOSSIER_ID: "TST01-123456",

  // For Demo purposes the next constants are defined to show the working. In real life secanrio's the number ranges must
  // match the number range of the donor/recipient and als the timestamp should be generated before the message is sent.
  START_NUMBER_RANGE: "0303800007",
  END_NUMBER_RANGE: "0303800007"
});
