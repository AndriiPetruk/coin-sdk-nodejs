# Changelog

## Version 1.1.0

Added:

- Support for Number Portability v3; adds the contract field to porting requests
  
Changed:

- Support Node 14 LTS
- SDK implementation now uses ES modules, but library is still published as CJS
- Deprecated `request` package got replaced with `got`
- Miscellaneous dependency upgrades
